from flask import render_template, request
#from models import  app, db, Book
from create_db import app, db, Book, create_books


#books = [{'title': 'Software Engineering', 'id': '1'}, {'title':'Algorithm Design', 'id':'2'},{'title':'Python', 'id':'3'}]

@app.route('/')
def index():
	return render_template('index.html')
	
@app.route('/book/')
def showBooks():
	books = db.session.query(Book).all()
	return render_template('books.html', books = books)

if __name__ == "__main__":
	app.run()

